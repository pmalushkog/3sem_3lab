#ifndef INC_2K_LAB_3_FINAL_DIRECTEDGRAPHANDDEIJKSTRA_H
#define INC_2K_LAB_3_FINAL_DIRECTEDGRAPHANDDEIJKSTRA_H
#include "Dynamic_Array.h"

// * Here T is the type of the vertex data and T1
// * is the type of the Arc weight.
template <typename T, typename T1> class DirGraph {
private:
    struct vertex;

    struct DirArc {
        vertex *startVertex;
        vertex *endVertex;
        T1 weight;

        // Constructors
        DirArc(vertex *start, vertex *end, T1 weight) {
            this->startVertex = start;
            this->endVertex = end;
            this->weight = weight;
        };

        DirArc(const DirArc &) = default;

        // Operators
        bool operator==(const DirArc &arc) {

            return (this->startVertex == arc.startVertex && this->endVertex == arc.endVertex && this->weight == arc.weight);
        };


        ~DirArc() {
            startVertex.~vertex();
            endVertex.~vertex();
        };
    };

    struct vertex {
        T data;
        DynamicArray<DirArc *> inArcs;
        DynamicArray<DirArc *> outArcs;
        T1 distance;
        vertex *previous;


        // Constructors
        vertex(T data) : data(data), previous(nullptr){};

        vertex(const vertex &) = default;

        // Operators
        bool operator==(const vertex &vert) {return this->data == vert.data; };

        ~vertex() {
            inArcs.~DynamicArray();
            outArcs.~DynamicArray();
        };
    };

    DynamicArray<vertex *> vertexes;
    DynamicArray<DirArc *> arcs;

public:
    // Constructors
    DirGraph() {
        this->vertexes = DynamicArray<vertex *>();
        this->arcs = DynamicArray<DirArc *>();


    };

    DirGraph(size_t vert_num, size_t arc_num) {
        this->vertexes = DynamicArray<vertex *>(vert_num);
        this->arcs = DynamicArray<DirArc *>(arc_num);

    };

    DirGraph(const DirGraph &other) { *this = other; };

    DirGraph<T, T1> &operator=(const DirGraph<T, T1> &other) {
        this->vertexes = other.vertexes;
        this->arcs = other.arcs;

        return *this;
    }

    ~DirGraph() = default;

    // Methods
    void AddVertex(T data) {
        this->vertexes.append(new vertex(data));
    };

    DirArc *AddArc(T1 weight, size_t fst = 0, size_t snd = 1, int id = 0) {
        if (this->CountVertexes() < 2) {
            throw std::out_of_range("There are less than 2 vertexes in the graph!!!");
        }
        auto arc = this->BindVertexes(this->vertexes[fst], this->vertexes[snd], weight);
        return arc;
    }

    bool ArcExists(size_t fst = 0, size_t snd = 1) {
        return this->ArcCheck(this->vertexes[fst], this->vertexes[snd]);
    }

    void allArcs() {
        int i = 1;
        for (const auto &elem : arcs) {
            std::cout <<"Дуга " << i << " { " << elem->startVertex->data << ", " << elem->endVertex->data << " }" << " Вес: " << elem->weight <<  std::endl;
            i++;

        }
    }

//    bool ArcExists_menu(size_t fst = 0, size_t snd = 1) {
//        return this->ArcCheck_menu(this->vertexes[fst], this->vertexes[snd]);
//    }

    size_t CountVertexes() { return this->vertexes.getSize(); }

    size_t CountArcs() { return this->arcs.getSize(); }

    void Elems_array(size_t start, size_t end) {
        auto vec = this->GetShortestPath(this->vertexes[start], this->vertexes[end]);

        int a[vec.getSize()+1];
        a[0] = start;
        a[vec.getSize()] = end;

        for (int i = 1; i < vec.getSize(); i++) {
            a[vec.getSize() - i] = vec.get(i)->endVertex->data;
        }

        for (int i = 0; i < vec.getSize()+1; i++){
            std::cout << a[i] << " ";
        }
        std::cout << std::endl;
    }


    T1 ShortestPath(size_t start, size_t end) {
        auto vec = this->GetShortestPath(this->vertexes[start], this->vertexes[end]);
        if (vec.getSize() == 0) {
            return std::numeric_limits<T1>::max();
        }

        T1 sum = T1();
        for (const auto &elem : vec) {
            sum += elem->weight;
        }
        return sum;
    }

    // ! Private methods
private:
    DirArc *BindVertexes(vertex *start, vertex *end, T1 weight) {
        if (this->ArcCheck(start, end)) {
            return nullptr;
        }
        auto arc = new DirArc(start, end, weight);
        start->outArcs.append(arc);
        end->inArcs.append(arc);
        this->arcs.append(arc);

        return arc;
    };


    bool ArcCheck(vertex *vert1, vertex *vert2) {
        for (const auto &elem: arcs) {
            if ((elem->startVertex == vert1 && elem->endVertex == vert2)) {
                return true;
            }
        }
        return false;
    }





//    bool ArcCheck_menu (vertex *vert1, vertex *vert2){
//    for (const auto &elem : arcs) {
//
//        if (elem->startVertex == vert1 && elem->endVertex == vert2) {
//            return true;
//        }
//    }
//    return false;
//};

    template <class R> void Delete(R data, DynamicArray<R> &array) {
        for (size_t i = 0; i < array.getSize(); i++) {
            if (array[i] == data) {

                array.eraser(i);

                return;
            }
        }
        std::runtime_error("There is no such element in this vector");
    }

    DynamicArray<DirArc *> GetShortestPath(vertex *start, vertex *end) {
        start->previous = nullptr;
        DynamicArray<vertex *> subgraph = DynamicArray<vertex *>(vertexes);
        DynamicArray<DirArc *> shortest = DynamicArray<DirArc *>();

        // Set the "infinite" distance for every vertex except of the start
        for (const auto &elem : subgraph) {
            if (elem == start) {
                elem->distance = T1();
            } else {
                elem->distance = std::numeric_limits<T1>::max();
            }
        }

        while (subgraph.getSize() > 0) {
            T1 min = std::numeric_limits<T1>::max();
            vertex *smallest = nullptr;

            for (const auto &vert : subgraph) {
                if (vert->distance < min) {
                    min = vert->distance;
                    smallest = vert;
                }
            }

            if (!smallest)
                break;

            auto adjacentVertexes = DynamicArray<vertex *>();
            auto adjacentDirArcs = smallest->outArcs;

            for (const auto &arc : adjacentDirArcs) {
                if (smallest != arc->endVertex) {
                    adjacentVertexes.append(arc->endVertex);
                }
            }

            for (size_t i = 0; i < adjacentVertexes.getSize(); i++) {
                auto adj = adjacentVertexes[i];
                T1 distance = adjacentDirArcs[i]->weight + smallest->distance;

                if (distance < adj->distance) {
                    adj->distance = distance;
                    adj->previous = smallest;
                }
            }

            Delete(smallest, subgraph);

        }

        // Making path
        vertex *previous = end->previous;
        vertex *cur = end;
        while (previous) {
            auto DiarcsInDiNode = cur->inArcs;
            for (const auto &arc : DiarcsInDiNode) {
                if (arc->startVertex == previous || arc->endVertex == previous) {
                    shortest.append(arc);
                    break;
                }
            }

            cur = previous;
            previous = previous->previous;
        }

        return shortest;
    }

    void allArcs(DirGraph<T,T1>* graph) {
        graph->arcs->startVertex;
    }

};
template <typename T, typename T1>
void TimeOfFoo(DirGraph<T,T1>* graph, int oper, int fst, int snd) {

    auto begin = std::chrono::steady_clock::now();
    if (oper == 1){
        graph->CountVertexes();
        graph->CountArcs();
        //graph.allArcs();
    } else if (oper == 2){
        graph->ShortestPath(fst, snd);
    }
    auto end = std::chrono::steady_clock::now();
    auto elapsed_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    std::cout << "The time: " << elapsed_ns.count() << " ns\n";

}
#endif //INC_2K_LAB_3_FINAL_DIRECTEDGRAPHANDDEIJKSTRA_H
